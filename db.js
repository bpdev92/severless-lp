const Sequelize = require('sequelize')
const ArtistModel = require('./models/Artist')
const VendorModel = require('./models/Vendor')
const TechniqueModel = require('./models/Technique')
const ManufacturerModel = require('./models/Manufacturer')
const PrintModel = require('./models/Print')

const sequelize = new Sequelize(
  process.env.DB_DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    dialect: process.env.DB_CONNECTION,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT
  }
)

//models 
const Artist = ArtistModel(sequelize, Sequelize)
const Vendor = VendorModel(sequelize, Sequelize)
const Technique = TechniqueModel(sequelize, Sequelize)
const Print = PrintModel(sequelize, Sequelize)
const Manufacturer = ManufacturerModel(sequelize, Sequelize)

const Models = { Artist, Vendor, Technique, Print, Manufacturer }

//relationships
Print.belongsTo(Artist, {foreignKey: 'artist_id', targetKey: 'id'})
Print.belongsTo(Vendor, {foreignKey: 'vendor_id', targetKey: 'id'})
Print.belongsTo(Technique, {foreignKey: 'technique_id', targetKey: 'id'})
Print.belongsTo(Manufacturer, {foreignKey: 'manufacturer_id', targetKey: 'id'})

const connection = {}

module.exports = async () => {
  if (connection.isConnected) {
    console.log('=> Using existing connection.')
    return Models
  }

  await sequelize.sync()
  await sequelize.authenticate()
  connection.isConnected = true
  console.log('=> Created a new connection.')
  return Models
}