
function transformArtist(artist) {
  return {
    "id":  artist.id,
    "name": artist.name,
    "created_at": artist.created_at,
    "updated_at": artist.updated_at
  }
}

function transformArtists(artists) {
  
  transformedArtists = []

  artists.forEach((artist) => {
    transformedArtists.push(transformArtist(artist))
  });

  return transformedArtists
}

module.exports = {
  transformArtist,
  transformArtists
};