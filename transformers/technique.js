
function transformTechnique(technique) {
  return {
    "id":  technique.id,
    "name": technique.name,
    "created_at": technique.created_at,
    "updated_at": technique.updated_at
  }
}

function transformTechniques(techniques) {
  
  transformedTechniques = []

  techniques.forEach((technique) => {
    transformedTechniques.push(transformTechnique(technique))
  });

  return transformedTechniques
}

module.exports = {
  transformTechnique,
  transformTechniques
};