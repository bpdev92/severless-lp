
function transformPrint (print) {
  return {
    "id":  print.id,
    "title": print.title,
    "artist": print.Artist,
    "edition": print.edition,
    "edition_size": print.edition_size,
    "technique": print.Technique,
    "manufacturer": print.Manufacturer,
    "vendor": print.Vendor,
    "picture": print.picture,
    "original_price": print.original_price,
    "release_date": print.release_date,
    "width": print.width,
    "height": print.height,
    "status": print.status,
    "edition": print.edition,
    "created_at": print.created_at,
    "updated_at": print.updated_at
  }
}

function transformPrints(prints) {
  
  transformedPrints = []

  prints.forEach((print) => {
    transformedPrints.push(transformPrint(print))
  });

  return transformedPrints
}



module.exports = {
  transformPrint,
  transformPrints
};