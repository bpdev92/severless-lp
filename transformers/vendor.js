
function transformVendor(vendor) {
  return {
    "id":  vendor.id,
    "name": vendor.name,
    "established": vendor.established,
    "location": vendor.location,
    "created_at": vendor.created_at,
    "updated_at": vendor.updated_at
  }
}

function transformVendors(vendors) {
  
  transformedVendors = []

  vendors.forEach((vendor) => {
    transformedVendors.push(transformVendor(vendor))
  });

  return transformedVendors
}

module.exports = {
  transformVendor,
  transformVendors
};