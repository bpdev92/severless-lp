const connectToDatabase = require('../../db')
const {CapitalizeStringWords} = require('../../helpers/Helpers')
const {transformArtist} = require('../../transformers/artist')
const uuidv1 = require('uuid/v1')

module.exports.create = async (event) => {
  try {
    data = JSON.parse(event.body);

    let params = {
      id: uuidv1(),
      name: CapitalizeStringWords(data.name),
      created_at: Date.now(),
      updated_at: Date.now()
    }

    const { Artist } = await connectToDatabase()
    const artist = await Artist.create(params)

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformArtist(artist)})
    }
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify({'error': 'Could not create the artist.'})
    }
  }
}
