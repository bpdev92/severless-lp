const connectToDatabase = require('../../db')
const {transformArtist} = require('../../transformers/artist')

module.exports.delete = async (event) => {
  try {
    const { Artist } = await connectToDatabase()
    const artist = await Artist.findById(event.pathParameters.id)

    if (!artist) 
      return {statusCode: 404 , body: JSON.stringify({data:{ error: 'Artist was not found' }})}
    
    artist.destroy()

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformArtist(artist)})
    }  
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify(err.data)
    }
  }
}
