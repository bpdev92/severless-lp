const connectToDatabase = require('../../db')
const {transformArtist} = require('../../transformers/artist')

module.exports.get = async (event) => {
  try {
    const { Artist } = await connectToDatabase()
    const artist = await Artist.findById(event.pathParameters.id)

    if (!artist) 
      throw {'statusCode': 404 ,'data':{ 'error': 'Artist was not found' }}
      
    return {
      statusCode: 200,
      body: JSON.stringify({data: transformArtist(artist)})
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
