const connectToDatabase = require('../../db')
const {CapitalizeStringWords} = require('../../helpers/Helpers')
const {transformArtist} = require('../../transformers/artist')

module.exports.update = async (event, context, callback) => {

  const data = JSON.parse(event.body);
  // validation


  const params = {
    name: CapitalizeStringWords(data.name),
    updated_at: Date.now()
  };

  const { Artist } = await connectToDatabase()
  let artist = await Artist.findById(event.pathParameters.id)

  if (!artist) {
    return {statusCode: 404 , body: JSON.stringify({data:{ error: 'Artist was not found' }})}
  }

  try {
    artist.update(params)
  } catch(e){
    return {
      statusCode: 500,
      body: JSON.stringify({'error': 'Could not update the artist.'})
    }
  }


  return {
    statusCode: 200,
    body: JSON.stringify({data: transformArtist(artist)})
  }


};