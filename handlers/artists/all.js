const connectToDatabase = require('../../db')
const {transformArtists} = require('../../transformers/artist')

module.exports.all = async (event) => {
  try {
    const { Artist } = await connectToDatabase()
    const artists = await Artist.findAll({order:[['name', 'ASC']]})

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformArtists(artists)})
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
