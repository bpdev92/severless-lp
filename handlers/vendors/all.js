const connectToDatabase = require('../../db')
const {transformVendors} = require('../../transformers/vendor')

module.exports.all = async (event) => {
  try {
    const { Vendor } = await connectToDatabase()
    const vendors = await Vendor.findAll({order:[['name', 'ASC']]})
      
    return {
      statusCode: 200,
      body: JSON.stringify({data: transformVendors(vendors)})
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
