const connectToDatabase = require('../../db')
const {CapitalizeStringWords} = require('../../helpers/Helpers')
const {transformVendor} = require('../../transformers/vendor')


module.exports.update = async (event, context, callback) => {

  const data = JSON.parse(event.body);
  // validation

  const params = {
    name: CapitalizeStringWords(data.name),
    established: data.established,
    location: data.location,
    updated_at: Date.now()
  };

  const { Vendor } = await connectToDatabase()
  let vendor = await Vendor.findById(event.pathParameters.id)

  if (!vendor) {
    return {statusCode: 404 , body: JSON.stringify({data:{ error: 'Vendor doest not exist.' }})}
  }

  try {
    vendor.update(params)
  } catch(e){
    return {
      statusCode: 500,
      body: JSON.stringify({'error': 'Could not update the vendor.'})
    }
  }

  return {
    statusCode: 200,
    body: JSON.stringify({data: transformVendor(vendor)})
  }

};