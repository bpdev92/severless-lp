const connectToDatabase = require('../../db')
const {transformVendor} = require('../../transformers/vendor')

module.exports.delete = async (event) => {
  try {
    const { Vendor } = await connectToDatabase()
    const vendor = await Vendor.findById(event.pathParameters.id)

    if (!vendor) 
      return {statusCode: 404 , body: JSON.stringify({data:{ error: 'Vendor was not found.' }})}
    
    vendor.destroy()

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformVendor(vendor)})
    }  
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify(err.data)
    }
  }
}
