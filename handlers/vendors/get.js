const connectToDatabase = require('../../db')
const {transformVendor} = require('../../transformers/vendor')


module.exports.get = async (event) => {
  try {
    const { Vendor } = await connectToDatabase()
    const vendor = await Vendor.findById(event.pathParameters.id)

    if (!vendor) 
      throw {'statusCode': 404 ,'data':{ 'error': 'Vendor was not found' }}
      
    return {
      statusCode: 200,
      body: JSON.stringify({data: transformVendor(vendor)})
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
