const connectToDatabase = require('../../db')
const {CapitalizeStringWords} = require('../../helpers/Helpers')
const uuidv1 = require('uuid/v1');
const {transformVendor} = require('../../transformers/vendor')


module.exports.create = async (event) => {
  try {
    data = JSON.parse(event.body);

    //validation

    let params = {
      id: uuidv1(),
      name: CapitalizeStringWords(data.name),
      established: data.established,
      location: data.location,
      created_at: Date.now(),
      updated_at: Date.now()
    }

    const { Vendor } = await connectToDatabase()
    const vendor = await Vendor.create(params)

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformVendor(vendor)})
    }
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify({'error': 'Could not create the vendor.'})
    }
  }
}
