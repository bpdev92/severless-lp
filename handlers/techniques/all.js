const connectToDatabase = require('../../db')
const {transformTechniques} = require('../../transformers/technique')


module.exports.all = async (event) => {
  try {
    const { Technique } = await connectToDatabase()
    const techniques = await Technique.findAll({order:[['name', 'ASC']]})

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformTechniques(techniques)})
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
