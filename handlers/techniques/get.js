const connectToDatabase = require('../../db')
const {transformTechnique} = require('../../transformers/technique')

module.exports.get = async (event) => {
  try {
    const { Technique } = await connectToDatabase()
    const technique = await Technique.findById(event.pathParameters.id)

    if (!technique) 
      throw {'statusCode': 404 ,'data':{ 'error': 'Technique was not found' }}
      
    return {
      statusCode: 200,
      body: JSON.stringify({data: transformTechnique(technique)})
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
