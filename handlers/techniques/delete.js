const connectToDatabase = require('../../db')
const {transformTechnique} = require('../../transformers/technique')

module.exports.delete = async (event) => {
  try {
    const { Technique } = await connectToDatabase()
    const technique = await Technique.findById(event.pathParameters.id)

    if (!technique) 
      return {statusCode: 404 , body: JSON.stringify({data:{ error: 'Technique was not found.' }})}
    
    technique.destroy()

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformTechnique(technique)})
    }  
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify(err.data)
    }
  }
}
