const connectToDatabase = require('../../db')
const {CapitalizeStringWords} = require('../../helpers/Helpers')
const {transformTechnique} = require('../../transformers/technique')

module.exports.update = async (event, context, callback) => {

  const data = JSON.parse(event.body);
  // validation

  const params = {
    name: CapitalizeStringWords(data.name),
    updated_at: Date.now()
  };

  const { Technique } = await connectToDatabase()
  let technique = await Technique.findById(event.pathParameters.id)

  if (!technique) {
    return {statusCode: 404 , body: JSON.stringify({data:{ error: 'Technique was not found' }})}
  }

  try {
    technique.update(params)
  } catch(e){
    return {
      statusCode: 500,
      body: JSON.stringify({'error': 'Could not update the technique.'})
    }
  }


  return {
    statusCode: 200,
    body: JSON.stringify({data: transformTechnique(technique)})
  }


};