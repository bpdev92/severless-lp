const connectToDatabase = require('../../db')
const {CapitalizeStringWords} = require('../../helpers/Helpers')
const uuidv1 = require('uuid/v1');
const {transformTechnique} = require('../../transformers/technique')


module.exports.create = async (event) => {
  try {
    data = JSON.parse(event.body);

    let params = {
      id: uuidv1(),
      name: CapitalizeStringWords(data.name),
      created_at: Date.now(),
      updated_at: Date.now()
    }

    const { Technique } = await connectToDatabase()
    const technique = await Technique.create(params)

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformTechnique(technique)})
    }
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify({'error': 'Could not create technique.'})
    }
  }
}
