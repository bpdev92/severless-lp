const connectToDatabase = require('../../db')
const {transformPrint} = require('../../transformers/print')

module.exports.get = async (event) => {
  try {

    const { Print, Artist, Technique, Vendor, Manufacturer } = await connectToDatabase()

    const print = await Print.findById(event.pathParameters.id,{
      include:[Artist, Technique, Vendor, Manufacturer]
    });

    if (!print) 
      throw {'statusCode': 404 ,'data':{ 'error': 'Print was not found' }}
      
    return {
      statusCode: 200,
      body: JSON.stringify({ data: transformPrint(print)}) 
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
