const connectToDatabase = require('../../db')
const {transformPrints} = require('../../transformers/print')

const PAGE_LIMIT = 16;

module.exports.index = async (event) => {
  try {

    data = JSON.parse(event.body);

    const { Print, Artist, Technique, Vendor, Manufacturer } = await connectToDatabase()

    let offset = (data['page'] * PAGE_LIMIT) - PAGE_LIMIT

    const prints = await Print.findAll({ 
      offset: offset, 
      limit: PAGE_LIMIT, 
      include:[Artist, Technique, Vendor, Manufacturer],
      order: [['created_at', 'DESC']], 
    })
    
    const total = await Print.count()

    let count = prints.length

    pagination = {
      'page': data['page'],
      'limit': PAGE_LIMIT,
      'count': count,
      'total': total,
      'totalPages': Math.floor(total/PAGE_LIMIT)
    }


//     'page' => $page,
//     'limit' => self::PAGE_LIMIT,
//     'count' => $prints->count(),
//     'total' => $total,
//     'totalPages' => floor($total/self::PAGE_LIMIT)
// ]

    return {
      statusCode: 200,
      body: JSON.stringify({data: transformPrints(prints), meta: pagination}) 
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
