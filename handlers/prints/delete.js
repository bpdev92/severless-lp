const connectToDatabase = require('../../db')

module.exports.delete = async (event) => {
  try {
    const { Print } = await connectToDatabase()
    const print = await Print.findById(event.pathParameters.id)

    if (!print) 
      return {statusCode: 404 , body: JSON.stringify({data:{ error: 'Print was not found.' }})}
    
    print.destroy()

    return {
      statusCode: 200,
      body: JSON.stringify({data: print})
    }  
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify(err.data)
    }
  }
}
