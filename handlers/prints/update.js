const connectToDatabase = require('../../db')
const {transformPrint} = require('../../transformers/print')

module.exports.update = async (event) => {
  try {

    data = JSON.parse(event.body);

    const { Print, Artist, Technique, Vendor, Manufacturer } = await connectToDatabase()

    const print = await Print.findById(event.pathParameters.id,{
      include:[Artist, Technique, Vendor, Manufacturer]
    });

    if (!print) 
      throw {'statusCode': 404 ,'data':{ 'error': 'Print was not found' }}


    let params = {
      'title': data['title'],
      'artist_id': data['artist_id'],
      'vendor_id': data['vendor_id'],
      'picture': data['picture'],
      'edition': data['edition'],
      'edition_size': data['edition_size'],
      'technique_id': data['technique_id'],
      'manufacturer_id': data['manufacturer_id'],
      'original_price': data['original_price'],
      'release_date': data['release_date'],
      'height': data['height'],
      'width': data['width'],
      'status': data['status']
    }

    print.update(params)

    return {
      statusCode: 200,
      body: JSON.stringify({ data: transformPrint(print)}) 
    }  
  } catch (err) {
    return {
      statusCode: err.statusCode,
      body: JSON.stringify(err.data)
    }
  }
}
