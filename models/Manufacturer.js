module.exports = (sequelize, type) => {
  return sequelize.define('Manufacturer', {
    id: {
      type: type.STRING,
      primaryKey: true,
    },
    name: {
      type: type.STRING,
      unique: true,
    },
    created_at: type.DATE,
    updated_at: type.DATE,
  },{
    timestamps: false,
    freezeTableName: true,
    tableName: 'manufacturers',
  })
}