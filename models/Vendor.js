module.exports = (sequelize, type) => {
  return sequelize.define('Vendor', {
    id: {
      type: type.STRING,
      primaryKey: true,
    },
    name: {
      type: type.STRING,
      unique: true,
    },
    established: type.STRING,
    location: type.STRING,
    created_at: type.DATE,
    updated_at: type.DATE,
  },{
    timestamps: false,
    freezeTableName: true,
    tableName: 'vendors',
  })
}