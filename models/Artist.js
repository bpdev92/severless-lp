module.exports = (sequelize, type) => {
  const Artist = sequelize.define('Artist', {
    id: {
      type: type.STRING,
      primaryKey: true,
    },
    name: {
      type: type.STRING,
      unique: true,
    },
    created_at: type.DATE,
    updated_at: type.DATE,
  },{
    timestamps: false,
    freezeTableName: true,
    tableName: 'artists',
  })

  return Artist

}