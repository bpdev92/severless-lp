// const Artist = require('./Artist')

module.exports = (sequelize, type) => {
  let Print =  sequelize.define('Print', {
    id: {
      type: type.STRING,
      primaryKey: true,
    },
    title: {
      type: type.STRING,
    },
    artist_id: {
      type: type.STRING,
    },
    picture: type.STRING,
    edition: type.STRING,
    edition_size: {
      type: type.STRING,
      allowNull: true,
    },
    technique_id: {
      type: type.STRING
    },
    manufacturer_id: {
      type: type.STRING
    },
    vendor_id: {
      type: type.STRING
    },
    original_price: type.STRING,
    release_date: {
      type: type.STRING,
      allowNull: true,
    },
    width: {
      type: type.STRING,
      allowNull: true,
    },
    height: {
      type: type.STRING,
      allowNull: true,
    },
    status: type.STRING,
    created_at: type.DATE,
    updated_at: type.DATE,
  },{
    timestamps: false,
    freezeTableName: true,
    tableName: 'prints',
  });


  return Print;
}